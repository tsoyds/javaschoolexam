package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import static java.lang.Double.parseDouble;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        if (statement == ""||statement == null) return null; //check for null string and empty string

        Stack<Character> stack = new Stack<>();

        StringBuilder sb = new StringBuilder();

        List<String> sbArr = new ArrayList<>();

        String l;

        char[] chNew = statement.toCharArray();

        if (hasWrongSymbols(chNew)==true) return null;

        for (int i = 0; i < chNew.length; i++) { //putting the numbers and symbols to the list with RPN
            switch (chNew[i]) {
                default:
                    if (i != (chNew.length - 1)) {
                        if (chNew[i + 1] == '+' || chNew[i + 1] == '-' || chNew[i + 1] == '/' || chNew[i + 1] == '*' ||
                                chNew[i + 1] == '(' || chNew[i + 1] == ')') {
                            l = sb.append(chNew[i]).toString();
                            sbArr.add(l);
                            sb.delete(0, sb.length());
                        } else {
                            sb.append(chNew[i]).toString();
                        }
                    } else if (i == chNew.length - 1) {
                        sbArr.add(sb.append(chNew[i]).toString());
                    }
                    break;
                case '(':
                    stack.push('(');
                    break;
                case '+':
                case '-':
                case '/':
                case '*':
                case ')':
                    stackWork(stack, chNew[i], sbArr);
                    break;
            }

        }
        while (!stack.empty()) {
            sbArr.add(String.valueOf(stack.pop()));
        }


        System.out.println(rpnEvaluate(sbArr));
        return (rpnEvaluate(sbArr));

    }
    private static void stackWork(Stack<Character> stack, char ch, List<String> sbArr) { // put symbols into stack
        //with RPN
        int opValue;
        int stackValue;
        char result;

        if (ch=='-'|| ch=='+') opValue = 1;
        else opValue = 2;

        if (stack.isEmpty()||stack.peek()=='('){
            stack.push(ch);
            result = '!';
        }
        else {
            if (ch==')'){
                while (stack.peek()!='(') { //when top of the stack is ')' , getting (pop) all stack before found '('
                    sbArr.add(String.valueOf( stack.pop()));
                }
                stack.pop();
                result = '!';
                return;

            }

            if (stack.peek() == '-'||stack.peek()=='+'){
                stackValue=1;
            }
            else stackValue=2;

            if (opValue<=stackValue) {
                result = stack.pop();
                stack.push(ch);
            }

            else {
                stack.push(ch);
                result = '!';
            }
        }

        if (result!='!'){
            sbArr.add(String.valueOf(result));
        }
    }

    private static String rpnEvaluate(List<String> list){ // this method gives list of RPN,
        // like 2,5,+,3,1,+,* -> (2+5)*(3+1)
        Stack<String> stack = new Stack<>();
        double result=0;
        double a,b;

        for (int i=0; i<list.size(); i++){

            switch (list.get(i)){

                default: stack.push(list.get(i));
                    break;
                case "*":

                    a=parseDouble(stack.pop());
                    b=parseDouble(stack.pop());
                    result = a*b;
                    stack.push(String.valueOf(result));
                    break;

                case "/": a=parseDouble(stack.pop());
                    if(a==0) return null;
                    b=parseDouble(stack.pop());
                    result = b/a;
                    stack.push(String.valueOf(result));
                    break;

                case "-": a=parseDouble(stack.pop());
                    b=parseDouble(stack.pop());
                    result = b-a;
                    stack.push(String.valueOf(result));
                    break;

                case "+": a=parseDouble(stack.pop());
                    b=parseDouble(stack.pop());
                    result = a+b;
                    stack.push(String.valueOf(result));
                    break;

                case"(":
                case")": return null;

            }
        }


        BigDecimal formattedDouble = new BigDecimal(result);
        String mainResult = formattedDouble.setScale(4, RoundingMode.HALF_UP)
                .stripTrailingZeros().toPlainString(); //convert to string #.####

        return mainResult;
    }

    private boolean hasWrongSymbols (char[] statementArr){ // the equation has // , **, ++, --, .. or ,
        for(int i=0; i<statementArr.length-1; i++){
            if (statementArr[i]==statementArr[i+1]){
                if ((statementArr[i]=='*')||(statementArr[i]=='/')||
                        (statementArr[i]=='-')||(statementArr[i]=='+')
                        ||(statementArr[i]=='.')) return true;
            }
            else if(statementArr[i]==',') return true;
        }
        return false;
    }

}