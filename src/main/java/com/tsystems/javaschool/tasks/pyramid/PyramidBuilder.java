package com.tsystems.javaschool.tasks.pyramid;

import java.util.Comparator;
import java.util.List;

public class PyramidBuilder {
    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        for(int i=0; i<inputNumbers.size(); i++){
            if (inputNumbers.get(i) == null) throw new CannotBuildPyramidException();
        } //checking for null

        if (inputNumbers.isEmpty()) throw new CannotBuildPyramidException(); //checking that list have numbers

        int i = inputNumbers.size(); //
        int counter = 0; //number of rows

        while(i>0){
            counter++;
            i -= counter;
        } // counting number of rows, deleting rows by arithmetic progression -1, -2, -3 ...-> then check

        if (i<0) throw new CannotBuildPyramidException(); //checking for symmetric pyramid

        inputNumbers.sort(Comparator.naturalOrder()); //sorting list inputNumbers

        int[][] currentArr = new int[counter][counter*2-1]; // new 2d array

        int n=0;

        for (int k=0; k<counter; k++){
            if (k==0)
                currentArr[k][(currentArr[k].length-1)/2] = inputNumbers.get(n++);
            else if(k>0) {
                for(int m=0; m<k+1; m++) {
                    currentArr[k][(currentArr[k].length-1) / 2 - k + 2*m] = inputNumbers.get(n++);
                }
            }
        } //filling 2d array with our inputNumbers

        return currentArr;
    }
}
